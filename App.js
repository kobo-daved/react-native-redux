import React, { PureComponent } from 'react';
import { Provider } from 'react-redux';

import Home from './src/components/Home/Home';

import store from './src/state/store';

export default class App extends PureComponent {
  render() {
    return (
      <Provider store={store}>
        <Home />
      </Provider>
    );
  }
}
