import { handleActions } from 'redux-actions';
import { increment, decrement } from '../actions';

const defaultState = {
    genericNumber : 1
};

export default handleActions({
    [increment](state) {
        return {
            ...state,
            genericNumber : state.genericNumber += 1
        }
    },
    [decrement](state) {
        return {
            ...state,
            genericNumber : state.genericNumber -= 1
        }
    }
}, defaultState);