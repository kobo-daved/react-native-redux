import { combineReducers } from 'redux';
import unaryReducer from './unaryReducer';

export default combineReducers({
    unaryReducer
});