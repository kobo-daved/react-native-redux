import { createStore } from 'redux';

import genericReducer from './reducers/genericReducer';

const store = createStore(
    genericReducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

export default store;