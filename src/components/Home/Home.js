import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

import { increment, decrement } from '../../state/actions';

class Home extends PureComponent {
    render() {
        return (
            <View style={styles.container}>
            <TouchableOpacity 
                style={styles.buttonStyle} 
                onPress={this.props.decrement}
            >
                <Text style={styles.textStyle}>Decrease</Text>
            </TouchableOpacity>        
            <TouchableOpacity 
                style={styles.buttonStyle} 
                onPress={this.props.increment}
            >
                <Text style={styles.textStyle}>Increase</Text>
            </TouchableOpacity>

            <Text>{this.props.genericNumber}</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container : {
        display: 'flex',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textStyle : {
        fontWeight: 'bold',
        color: '#fff'
    }, 
    buttonStyle : {
        borderWidth: 2,
        marginVertical: 25,
        borderColor: 'red',
        backgroundColor: '#000',
        borderRadius: 4,
        padding: 8,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    }
});

const mapStateToProps = (state) => ({
   genericNumber: state.unaryReducer.genericNumber
});

const mapDispatchToProps = ({
    increment,
    decrement
})

export default connect(mapStateToProps, mapDispatchToProps)(Home);