### Setup

Install react-native-cli globally
`npm install -g react-native-cli`

Install dependencies
`yarn install`

### Running

To run the application on an iPhone simulator:

    react-native run-ios